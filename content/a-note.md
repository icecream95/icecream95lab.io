Title: a note
Date: 2024-06-13
Summary: here you are, a note

I often mean to write something, and then spend a long time planning
that, but in the end find some reason to convince myself not to.

So this is an attempt to write something quickly and then post it
before I manage to convince myself not to.

<hr>

For the past while, I think most of two years now, I have been trying
to keep a low profile, not writing anything here or answering
questions from users of the GPU-driver related code I have written
(which itself I have recently taken down, in part since there are now
alternatives), etc.

I could give a number of reasons for this, depending on the day of the
week, but the one for today is related to people who seem to have
accused me of everything except for what I have actually done, which
is quite an achievement given how much there is in that excluded
class.

The original fear was that replying to those people or continuing to
attract their attention would just lead them to make more extreme
accusations based on their assumptions. 

Eventually, after I had already acted on an implied request for me to
stop developing my fork of Mesa, I decided to just try to disappear
completely, as a way to avoid that, and so that I wouldn't even know
what they thought about me.

(For that matter, I don't even know what (former?) users of my fork
think about me, abandoning them like I did. Maybe I should have said
more before doing that.)

<hr>

It's a long time since then.

I have learnt more about how friendship is magic, so perhaps better
understand all the non-magical things that can cause non-friendship.
More important perhaps is knowing that trying to avoid all magic is
not something to strive for.

I think that the list of what I consider to have done wrong has grown
since when I first had those accusations to compare with ("me? I was
not trying to be unfriendy, was there some mistake?"), and I think
that I have a lot to apologise for.

But I will not attempt to give an apology now; if I did, then I would
spend too much time trying to write it carefully and to prove every
lemma and theorem; I don't have the time (or energy) for that at this
immediate point in time.

I still think that this situation could have been handled better by
others, but I mustn't forget that it was my own... recklessness,
perhaps? that lead to all of this in the first place.

<hr>

I am running out of time, so I will post this before I realise what
I've just written. Apologies for the weird grammar here; I do that
when I am tired. I don't know how understandable this is for those
who do not know the context, but this isn't really written for you.

I don't really know my aim is with writing this; I guess I just want
to do something rather than feel bad about saying basically nothing in
the way of apology.
(It still feels like I haven't really said much, but I don't have any
more time either. There are certain things I want to be more clear
about, but I'm unsure how to put them when writing here.)

But will saying this do any good, or did the horse bolt long ago and is it all
[futility](https://manpages.debian.org/bookworm/vboot-kernel-utils/futility.1.en.html)?

It's anypony's guess.

*[See also [another note](another-note)]*

## About the author

No longer a teenager, Icecream95 has decided to stop hiding behind
having a low age. This does not mean that Icecream95 will stop hiding,
but perhaps, against all odds, this will not be the last post on this
blog.

Comments to `the.real.icecream95 at gmail.com`.
