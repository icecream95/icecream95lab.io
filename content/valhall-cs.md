Title: The Mali CSF Command Stream Instruction Set
Date: 2022-08-12
Category: RE

## Introduction

Starting with "v10", Arm's range of Valhall GPUs have dropped the old "job chain" mode of submission and have switched to a command stream. So whereas previous architectures involved interpreting hardcoded descriptors, CSF GPUs execute commands to build up the descriptors in the command stream register set, which get copied to another region of memory when the GPU job is started. This makes it easy to read and write memory to patch jobs before execution (though the only ALU operation is `ADD`), or execute jobs in a loop, as well as synchronising against other jobs without kernel involvement.


Command streams are interpreted by the "iterator", which communicates with the MCU and other areas of the GPU in order to execute many of the instructions, such as reading cycle counters, signaling events, or starting draws.

(I initially thought that the MCU evaluated the top level of command stream, but that does not appear to be the case. Indeed, it's even possible to launch graphics jobs directly from the CS ring buffer.)

Similar to a GPU core, the iterator can only execute a single command stream at once, but it switches between them after every instruction. Certain instructions can be pipelined, so a loop that takes 17 cycles when run on a single command stream might take 20 cycles on two command streams, rather than 34. 

## More about command streams

`kbase` provides `ioctl(KBASE_IOCTL_CS_GET_GLB_IFACE)` for retrieving information about the command streams, which includes the `STREAM_FEATURES_WORK_REGISTERS` register, in typical Mali fashion storing the number of work registers minus one. The `NO_MALI` feature in `kbase` lists 81 registers, but G610/G710 hardware has 96. It's possible that later GPU architectures bump this number up, but the maximum is 256.

The registers are likely stored in an internal register file, but are persistent between jobs, and call instructions/returns. It is unknown whether command-stream group eviction does anything to the registers. The iterator so far appears to use a flat register set, with no observable effects from poking random registers. The program counter / call stack registers are not immediately accessible.

After eight nested call instructions, the ninth fails with a `CS_CALL_STACK_OVERFLOW` error. If deeper nesting is required, a workaround could be to use self-modifying code to change return branch instructions.

## Scoreboards

The features register has other interesting fields: It lists the number of scoreboards (v10 supports 8), and also whether compute, fragment and tiler jobs are supported.

What are these scoreboards? Similar to Valhall-the-ISA, the CS instruction set appears to have a set of scoreboards which can be waited on. For example, `LDR` instructions are high latency and do not return the result immediately, so `WAIT 0` must be executed to wait on the zeroth scoreboard before the result register can be read. The other uses of scoreboards are not so clear:

There are a number of... odd conventions wrt scoreboarding that I have still not fully determined the meaning of. Usually the blob will do something like `SLOT 3; WAIT 3` before starting a GPU job, which means that there may be restrictions on how many work items can use the same scoreboard slot. I have also seen long streams of `SLOT 2; WAIT 2; SLOT 3; WAIT 3` repeated over and over again in some cases. It appears that the name `WAIT` might not be entirely accurate.

## Conventions

AArch64 conventions are mostly followed wherever relevant, except that registers are written in hex.

A slice of the register file:

| w1a | w1b | w1c | w1d | w1e | w1f | w20 | w21 |
|-----|-----|-----|-----|-----|-----|-----|-----|
| x1a |     | x1c |     | x1e |     | x20 |     |

Registers are "natively" 32-bit, but many instructions operate on 64-bit registers. So `x1a` is equal to `w1a + (w1b << 32)`.

Note that load and store instructions appear to be able to read or write odd-numbered 64-bit registers, but that is just treating it as a sequence of two 32-bit registers. 64-bit registers must normally be even-numbered.

Although instructions are written as uppercase, the assembler is case-insensitive and the disassembler uses lowercase.

Encodings are given as a single 64-bit value in big-endian order. So the hypothetical encoding `0x1122 3344 5566 7788` is stored to memory as the seqeunce of bytes `88 77 66 55 44 33 22 11`.

## Instructions

Note that this list is incomplete. In particular, the actual compute / IDVS / fragment launch instructions are missing. Go look at the source code if you want to learn about them, but they aren't actually that interesting.

Not all instructions are supported by the assembler, but `UNK <hex of instruction>` will work, for example `UNK 0400ff0000008001`.

Most "simple" instructions take 4 cycles.

Cycles are usually given for the G610 in RK3588 at 330 MHz (which appears as 300 MHz in `devfreq`), at higher frequencies instructions that access RAM can take a few cycles longer. But I think that many instructions that are slow spend most of their time in the MCU, which appears to run at a fraction of the GPU clock speed.

Instructions that require handling by the MCU may change behaviour with new firmware revisions. A `mali_csffw.bin` file with md5sum `a9c8d66988149968494d1f4c730272df` was used, which reports as version 1.1.

`NOP`
:    `0x0000 0000 0000 0000`

      Do nothing.

`MOV <Xd>, #<imm48>`
:    `0x01<Xd> <imm48>`

      Zero-extend the 48-bit immediate to 64-bits and write to the destination register.

`MOV <Wd>, #<imm32>`
:    `0x02<Wd> 0000 <imm32>`

     Write the 32-bit immediate to the destination register.

`WAIT <scoreboard mask>`
:    `0x0300 0000 00<scoreboard mask> 0000`

     Wait for operations to finish on the given scoreboards. Exactly *which* operations are waited for is unknown; it appears to not include GPU jobs.

     `WAIT 0` waits for `LDR`/`STR` to finish.

`ADD <Rd>, <Rn>, #<simm32>`
:    `opc` is 0x10 for the 32-bit variant, 0x11 for the 64-bit variant.

     `0x<opc><Rd> <Rn>00 <simm32>`

     Adds a 32-bit signed immediate to the source register and writes it to the destination register.

     The case where `simm32 == 0` is used for register-to-register move instructions.

`LDR <register list>, [<Xn>{, #<simm16>}]`
:    

`STR <register list>, [<Xn>{, #<simm16>}]`
:    `simm16 & 3` must be zero.

     `opc` is 0x14 for `LDR`, 0x15 for `STR`.

     `base` is the first register in the register list.

     `register mask` is a 16-bit mask of registers to operate on.

     `0x<opc><base> <Xn>00 <register mask> <simm16>`

     Loads or stores a set of 32-bit registers, where the mask specifies as many as 16 registers to operate on. These registers are offset from a base register, so the highest register can be up to 15 registers onwards from the base.

     For example, `LDR {w05, w09, x1c}, [x06, 12]` will have a base register `w05`, a mask of `00000001 10010001`, and load from these addresses:

     | w05      | w09      | w1c      | w1d      |
     |----------|----------|----------|----------|
     | x06 + 12 | x06 + 16 | x06 + 20 | x06 + 24 |

     Note the use of `x1c` as a shorthand for `w1c, w1d`.

     There is no alignment requirement on the base register number. `simm16` must be aligned to 4 bytes. The two low bits in the address register are discarded. The address register is not updated. Load instructions may not load to any part of the address register.

     These instructions are high latency. `WAIT 0` must be executed to wait for the result of `LDR`.

`B <branch offset>`
:    

`B.<cond> <Wn>, <branch offset>`
:    For the `B` instruction, `cond` is `.AL`, and `Wn` is unused.

     This is the table of supported conditions:

     | `.LE` | `.GT` | `.EQ` | `.NE` | `.LT` | `.GE` | `.AL` |
     |-------|-------|-------|-------|-------|-------|-------|
     | Less than or equal | Greather than | Equal | Not equal | Less than | Greater than or equal | Always |
     |  0x00 |  0x10 |  0x20 |  0x30 |  0x40 |  0x50 |  0x60 |

     `0x1600 <Wn>00 <cond>00 <branch offset>`

     Compare the signed register `Wn` against zero, and branch if the condition holds.

     The disassembler uses a ones-complement decoding for the 16-bit branch offset. If it is positive, it prints `skip <N>`, otherwise `back <N>` where `N` is the bitwise inverse of the offset.

     An example showing how "local labels" are used by the assembler:

         B 2f                  @ B w00, SKIP 1
         3: ADD w15, w15, -1
         2: B.ne w15, 3b       @ B.ne w15, BACK 1. Jumps if w15 != 0

     Branch instructions take 13 cycles.

     Note that branching to another cache line appears to flush the instruction cache, so it can be expensive (~190 cycles). Tight loops should keep within a single cache line. It seems that this might only apply to backwards jumps.

`SLOT #<slot>`
:    `0x1700 0000 0000 00<slot>`

     Selects the scoreboard to use for certain high-latency instructions. Often `SLOT` will be follwed by `WAIT` on the same slot.

`CALL <Wm>, <Xn>`
:    

`TAILCALL <Wm>, <Xn>`
:    `opc` is 0x20 for `CALL`, 0x21 for `TAILCALL`.

     `0x<opc>00 <Xn><Wm> 0000 0000`

     Branches execution to `Xn`, and returns when `PC` reaches `Xn + Wm`.

     So if the value of the register `Wm` is 8, then only a single instruction will be interpreted.

     On v10 GPUs, the call stack is a maximum of eight levels deep. `TAILCALL` reuses the current stack frame.

`ENDPT <modes>`
:    `0x2200 0000 0000 <modes>`

     Specifies a mask of endpoints that may be used.

     | Name | Value |
     |------|-------|
     | `compute` |  1 |
     | `fragment` | 2 |
     | `vertex1` |  4 |
     | `vertex2` |  8 |

     It appears that this can be changed at any time, though doing so might be expensive.

     The difference between `vertex1` and `vertex2` is unknown—presumably one is for IDVS and the other is for pure tiler jobs.

     If a CS tries to execute a disallowed job type, it will hang.

`EVADD <Rt>, [<Xn>], UNK <unk>, FLAGS <flags>`
:    

`EVSTR <Rt>, [<Xn>], UNK <unk>, FLAGS <flags>`
:    For 32-bit variant: `opc` is 0x25 for `EVADD`, 0x26 for `EVSTR`.

     For 64-bit variant: `opc` is 0x33 for `EVADD`, 0x34 for `EVSTR`.

     `0x<opc>01 <Xn><Rt> 00<unk> 00<flags>`

     Either stores to the destination, or performs an addition and stores the result (the source register is unchanged). The latter is the only known way to perform an addition of two variables, discounting self-modifying code.

     Note that although the addition may be atomic against other command streams, it will not be atomic when also written by other parts of the system, including GPU atomic memory operations.

     The flag bits are still partially unknown. Starting from LSB:

     - Bit 0 is usually set by the blob, and does an unknown extra operation. Disassembled as `unk0` if set.
     - Bit 1 must be zero to avoid an invalid instruction fault
     - Bit 2 is set to disable sending an IRQ to the CPU about the event

     If bit 2 is unset, the destination *must* be to memory allocated with the `BASE_MEM_CSF_EVENT` flag, otherwise the result will not be stored to memory. However, events are still signaled. When bit 2 is set, this restriction appears to not apply.

     The destination must be 8-byte aligned for the 32-bit variant, and 16-byte aligned for the 64-bit variant. Four bytes are written after the destination, which have so far been observed to be zero.

     Even when not signaling an IRQ, `EVSTR` can take several thousand cycles, and `EVADD` is about 30% slower than that. In the best case and when run in a loop, `EVSTR` is almost 700 cycles. Whatever bit 0 does takes another 100 cycles, and signaling an IRQ takes about 500 cycles.

     Other command streams stuck at `EVSTR` will be woken up, even if they are waiting on a different event. They are woken several hundred cycles before the `EVADD` instruction returns, but this can vary a lot.

`EVWAIT.<cond> <Rt>, [<Xn>]`
:    `opc` is 0x27 for the 32-bit variant, 0x35 for the 64-bit variant.

     The value of `cond` is 0 for `.LS`, and 1 for `.HI`.

     `0x<opc>00 <Xn><Rt> <cond>00 0000`

     Waits until the condition is true when comparing `Rt` to the value in memory. For example, with `.HI` the value in memory must be (unsigned) higher than `Rt`. The other comparison operation is `.LS`, unsigned less than or same.

     If the comparison is false, it is not checked again until the iterator receives an event, such as after every `EVSTR` instruction.

`STR timestamp, [<Xn>{, #<simm16>}]`
:    

`STR cycles, [<Xn>{, #<simm16>}]`
:    `type` is 0 for `timestamp`, 1 for `cycles`.

     `0x2800 <Xn><type> 0000 <simm16>`

     Stores a 64-bit timer value to the address register at the given offset.

     The destination address must be 8-byte aligned, and the bottom three bits of the immediate value are ignored.

     Either the cycle counter (which is reset when the GPU is powered off) or the system timestamp is used.

     The timestamp counter should update at the rate of `CNTFREQ_EL0`, which is usually 24 MHz. The cycle counter updates at the GPU frequency. Note that the GPU frequency may differ from what is reported in devfreq; for the RK3588 vendor kernel, what is reported as 300 MHz is actually 330 MHz!

## Self-modifying code

Here is a program for `interpret.py` demonstrating self-modifying code.

It should be about as expensive as the branch across the cache line; maybe 250 cycles or so?

The only special part is the cross-cache-line branch; they invalidate the instruction cache, so the branch destination and any later instructions are loaded from memory again.

```text
!cs 0
!alloc x 4096

@ Call destination/length, will be filled in by relocation handling and
@ the assembler, respectively
mov x48, 0
mov w4a, 0

@ interpret.py doesn't relocate the location correctly for the main CS
@ ring buffer, we must call to another buffer
call w4a, x48

  @ Padding; the branch must straddle a cache line
  nop
  nop
  nop

  @ Relocated to the location of the current instruction buffer
  mov x20, $.
  @ movp is a psuedo-instruction for a 64-bit immediate load; it is split
  @ into two 32-bit loads.
  @ The payload is a mov instruction itself
  movp x22, 0x0124000044332211
  
  @ Write the payload into the address of the branch instruction
  str x22, [x20, 64]
  
  @ Padding so that the branch instruction can go back to the last
  @ cache line
  1: nop
  
  @ Branching across a cache-line seemingly invalidates the instruction cache
  @ This branch only needs to jump once before it is replaced
  b 1b
  
  @ Now the replaced instruction has executed, store our new register to memory
  mov x40, $x
  str x24, [x40]

!dump x 0 4
```

## Pseudo-instructions

The assembler supports a few pseudo-instructions.

`!CS <N>`
:    

`!PARALLEL <N>`
:    Selects a command stream to specify commands for.

     The `csf_test` executor creates four command streams, and picks default endpoints for them, though that can be changed with the `ENDPT` instruction.
     
     `PARALLEL` is similar to `CS`, except that it executes in parallel with the previous `CS` or `PARALLEL` section.

`!ALLOC <name> <size> {0x<flags>}`
:     Allocates memory. If `flags` are not specified, the default is `0x200f`, allowing CPU and GPU reads and writes. (The iterator doesn't require command stream memory to be executable.)

     To allocate event memory, use `0x8200f`.

`!DUMP <name> <offset> <size>`
:     

`!DELTA <name> <offset> <size>`
:    Dump the memory region called `name` starting at `offset` bytes.

     `!DUMP` will perform a hex dump, but `!DELTA` prints the difference between each set of eight bytes in memory. The purpose of this is that a loop can be used to store the cycle counter to successive locations, making it simple to count the number of cycles taken for a section of command stream.

`MOVP <Xn>, #<imm64>`
:    Using two 32-bit move instructions, write the 64-bit immediate to the destination register.

     For 48 or fewer bits use a 64-bit `MOV` instead.

`REGDUMP <Xn>`
:    This instruction expands to six store instructions, storing the entire G610/G710 register set to memory starting at `Xn`. This is 384 bytes.

## Executor syntax

The `interpret.py` script and `csf_test` executor use a simple format internally for specifying commands.

Users of `interpret.py` do not need to know how to write this format.

`reloc <dst>+<offset> <src>+<src_offset>`
:    Apply a relocation. Add `src_offset` to the address of buffer `src`, and do a 64-bit ORR to the contents of the byte offset `offset` of the buffer `dst`.

`buffer <dst> <size> <flags> <data...>`
:    Create a buffer with flags `flags` and size `size`, and store the data into it.

     `data...` must be a series of 64-bit hex values, for example: `0x1122334455667788 0x3 0x500000`. The length of the list must not be more than one eighth of `size`.

     It is possible to not specify any data, to only allocate zeroed memory.

`exe {<iter> <dst> <size>}...`
:    Execute from the start of buffer `dst` for `size` bytes, on CSI `iter`.

     Note that instructions are copied into the ring buffer, and not executed directly.

     Multiple tuples may be selected, but the command streams are always executed in numerical order. For example, `exe 1 10 64 0 7 128` will first execute the 128 bytes at the start of buffer seven on CSI 0, only afterwards (but while the first CS is still executing) starting CSI 1 with 64 bytes from buffer 10.

     After all specified command streams have been started, no further commands are executed until they have all finished.

`dump <src> <offset> <size> <mode>`
:    Dumps from `src` offset by `offset` bytes, for `size` bytes, using mode `mode`. Modes are:

     - `hex`: A standard hex-dump using `pan_hexdump`
     - `delta`: Print the difference between sets of eight bytes

     Note that unlike `reloc`, there is a space rather than `+` between `src` and `offset` to simplify implementation.

## fin.

So... where exactly *is* this `csf_test` executor, `interpret.py` script, disassembler, assembler, and so on?

I suppose you could take a lookie at <https://gitlab.com/panfork/mesa>, and while you're at it <https://gitlab.com/icecream95/panloader> and <https://gitlab.com/panfork/panfwost> might also be of interest.

It's a bit of a mess at the moment, because I'm too busy actually using it, and extending to allow exploration of new corners of the command stream ISA, to keep it clean.

Instructions on using the script are in the `README.rst` of the Mesa repo. At the moment, you'll want to edit the `cmds` string inside `interpret.py`, though this is subject to change.

## Endnotes

I'm, ah, hearing rumours about a certain v10 branch that accidentally got pushed and then was hastily deleted once the mistake was realised. If the perpetrator happens to chance across the blog post: Is it really that secret that it must not be public? It didn't seem very secret to me, just a pile of boring plubming code.

I think that the idea of queues should probably be kept far away from pandecode, but I do realise that my simple va + size API might be too limited. In any case, accurately decoding is a challenge. I had to disable compute launch decoding when starting to use shaders from `interpret.py` because I wrote some of the descriptors into memory from the command stream, so pandecode just read zeroes as this was before execution. Even when decoding after execution, the initial state of e.g. loop counters may be destroyed.

I do at least suspect that it will often be found useful to allow disassembling arbitrary sections of command stream without necessarily having to be linked into the CS ring buffer.

(If it's because of something I've done that it has to be secret, I'm sorry.)

## About the author

Although not taking advantage of being old enough to drink, the jury may still be out on whether Icecream95 is too young to develop GPU drivers. Icecream95 ignores that, and spends his time working on a fork of the Panfrost driver in Mesa for Arm Midgard, Bifrost, and Valhall GPUs, though right now Valhall is the main focus. Since his first reverse engineered feature to Mesa back when he could still write his age with a single hex digit, Icecream95 has wasted a lot of time on reverse engineering things that will never be of much use upstream. Almost three years later, he has discovered that there seems to be at least three different CRC algorithms used in Mali GPUs, though he only owns hardware for two variants.

Lightning McQueen also has 95 on his side, and certainly has reverse engineered some command stream ISAs used in the automotive sector, but Icecream95 does not remember the movies very well. This isn't the reason for Icecream95's username, though. 
When taking the sum of the letters of the alphabet in 'Icecream', adding the same with his login name, and removing 'I' and 'a', the result is 95. Not very convincing, I know, but there you have it.
