Title: a third note
Date: 2024-08-10
Summary: More about me and myself

*[See also the earlier [note](a-note) and [other note](another-note).]*

It's been about a month since I wrote those two previous notes.

Since then I haven't really done much. 
No, I didn't make my fork[^1] repositories private in order to
secretly work on a big update, or anything like that.

It's gone, totally unsupported, and since upstream[^2] now
has code for the GPUs for which my fork existed,
users should go there, so it can receive more testing
and become a better driver, rather than staying
with something that I *will not* update.

## in

I also have not done much towards trying to see a resolution
to this conflict,
except living and becoming more experienced
and hopefully better working out what to do
about difficult situations.
Oh and seeing Inside Out 2; would recommend but
won't (but could) write about it here.

On one hand I want to write and send emails apologising (as much as I can)
for what I have done.

But I am afraid to do that,
because I have convinced part of myself[^3] that,
since I have been accused of being manipulative,
everything I say will be interpreted in that context.
Since I have already (according to some)
pretended to be sorry,
surely the most likely thing is that I am doing it again?

Even worse, if anyone believes that I am not
trying to be manipulative, then that
in itself might be seen as evidence of
me being manipulative.[^4]

## side

Reading emails sent to me from when I was unbanned 
(after getting banned the first time) makes me cry.

> I believe behaviours can change if people make a sincere effort.

I am unsure if that belief was correct or not.

Sometimes it seems that what happened was inevitable,
and that there was no way that 18-year-old me could
have avoided this outcome.
I *knew* I was acting wrong.
I *tried* to talk about it.
I wanted to talk to the Code of Conduct
committee, but was afraid to,
or even to talk about wanting to,
so I decided that the only solution
to that was to mention it
in an easier-to-type way to
a friend I trusted.

So somehow I ended up writing something ambiguous where a reasonable
interpretation would be that I was, er, a gaslighting abuser.
Perhaps why I'm being accused of that is related.

For all my sincere efforts,
I only made things far worse.[^5]

(I've never read it in that way before.
It takes effort to think about how something might be interpreted
other than the meaning I intended to give it.
It's easy to look at the words and connect it to what was interpreted,
but difficult to convince myself that a reasonable person
could interpret it that way;
initially I thought it would require malicious misinterpretation.)

## or

Other times, I know that I would have
acted a lot better
had I been just a bit more careful,
not forgotten just a couple of things,
forgotten just a couple of others,
or been more careful with some
things I've learnt.

Also if I had replied properly
to the email I quoted from,
I might have been able to discuss
things enough to avoid the causes
of when I got banned again.

I don't have to say that it was impossible to be better.

I don't have to say that it was too difficult to talk
about how I could have been better.

Perhaps.

## now

Now I've looked at the past, let's go back to now.

Have I improved?

I think that to some degree, I must have, otherwise I would not
have been able to write all this.

But I'm still the same person.
In many ways I have not really changed.
If I was unbanned and things continued
similar to after the first time,
I suspect that it still might not be
long before I'd be here again.[^6]
And while I can write things like this at the moment
I suspect that I would find it too difficult to continue
if I was no longer in this position.

I would guess: I'm better at understanding and reasoning about things,
and can perhaps better plan what to do in different situations,
but execution is still where things fall down.

What else can I say about myself?

I still get quite worried when it seems like history is repeating
and I'm again in the same situation as before.
It can take very little for me to be convinced that someone
is only pretending to be friendly, but is waiting for
me to do the wrong thing, in order to then pounce.[^7]
(I wonder if the Code of Conduct committee was right to
give absolutely no warning before banning me the second time.)

It can take very little for me to be convinced of anything, really.
That's another problem with me that hasn't improved enough.

## outside

I think that's probably enough for now.
I wonder if I should structure these posts better.
I'm probably repeating myself too much.
I wonder if there's still some important insight
that I'm missing.[^8]
I know that I need to spend time considering how
I acted after getting banned,
since I was a lot worse than I should have been.[^9]

Why do I persist in thinking and writing
about this situation?

Perhaps: Because how things are at the moment makes me sad,
and I want to see a happy ending.

(Another reason, a little less noble, is that I'd like to no longer
worry about people popping up, describing how hostile I must be and
the "implicit legitimacy" that having anything to do with me gives me,
and then signing off with "Thanks :)".
I have not really contributed to any other Free Software projects
since getting banned from the Freedesktop community, party because
of that fear.)

A third reason is that other people might read this
and have interesting and useful thoughts:
Comments, unsolicited advice, admonishments, cautions, replies,
etc., to `the.real.icecream95 at gmail.com`.
Please don't be shy!

## p.s.

To Alyssa Rosenzweig, 
thank you again for your efforts,
even if (as I suggested in my last note)
they were doomed to not be enough.
Though it would be wrong to try to thank you
without acknowledging the bitter thanks
that was your reward at the time:[^10]
I was dishonest, and for whatever justification
I gave myself, it hurt you.
And afterwards,
while I did try to ask the Code of Conduct
committee about whether I should continue
developing my fork,
I did not weight the
obvious answer enough.[^11]

Surely this has all been unnecessary,
and surely I could have been brave enough
to be better.
But I ask that you would at least forgive
yourself for when you chose to work with me.

## About the author

Icecream95 has already written too much about this topic.[^12]

Instead, I'll note that yesterday I saw a horse from a bus that was
turning around at the end of a long dead-end street. I don't think
I've ever seen a horse from an urban bus before. (Though maybe I've
seen a pony, if I remember right?) I suspect that the horse has more
sense than I do, which is why it has not been banned from
freedesktop.org spaces.

[^1]: *panfork*, a fork of the Mesa userspace GPU driver project,
in this iteration modified to support Mali-G610 GPUs in the Panfrost
driver, which was actively developed during 2022.

[^2]: See <https://docs.mesa3d.org/drivers/panfrost.html>,
maybe ask on IRC about what kernel to use with it.

[^3]: Whenever I use language like that, it means
that it's probably irrational. But there's no easy
way to determine whether it is or not, is there?

[^4]: Note that I give the appearance of being
open and honest about myself, but I'm still using
a pseudonym  and there is no way to check most of
what I say. That's probably a red flag, and maybe
it is correct to be distrustful of me.

[^5]: What were my sincere efforts *towards*?
Trying to change behaviours, or trying to
avoid problems through brute application
of effort? Perhaps that's part of the problem.

[^6]: And if people that I associated with were less
permissive (and had less reason to trust me)
then I probably would keep finding myself in
similar situations.
But it's not reasonable to ask all people to be so permissive.
And I need to learn to be less assuming about what is okay.
(Or rather more assuming, but with more correct assumptions?)

[^7]: As recently as earlier this week I wrote to a friend
to explain why I had been acting oddly around her.

[^8]: I "wonder" if there are some important topics that I
should write about but am avoiding. (I don't mean the obvious
guesses, like whether Codes of Conduct should exist or not.)

[^9]: Read old posts on this blog and reach some of your own conclusions.

[^10]: Why? Because otherwise I'm effectively thanking her for hurting
herself, which is not a good thing to do in any context.

[^11]: Non-exhaustive list of misdeeds.
In short, I replied to kindness with unkindness.

[^12]: And *far* too many footnotes!
