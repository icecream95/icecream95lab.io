Title: another note
Date: 2024-06-17
Summary: more on a note

*[This is a follow-up to a [note](a-note)]*

I have still not been brave enough to check my email or IRC logs or
anything, so I don't know what the reaction to my last post has been,
or even whether anybody at all has read it; but here is some more
anyway.

Last time I wrote *"I will post this before I realise what I've just
written"*, since otherwise I might have decided to not do that. So
then, what *had* I written?

Well, I have been accused of being a "repeated abuser" who "pretended"
to have "understood" that my "behaviour was unacceptable". So from the
point of view of someone who believes that, what I wrote last week
could very well be just more pretension, meaning there is no reason to
believe a single word.

Having people think that is not all that bad; it's not something new
for me any more. Worse is my own realisation that *I* have written
something indistinguishable from abuse. Am I just hurting again
someone who was a friend? Have I not listened to being told to go
away, because I want to cause hurt *that much*?

But I think it is right to apologise for how I have acted. And any
apology (or foreword to an apology, as that post was) is going to
involve an understanding of what I've done wrong. So this similarity
is inevitable, and not really proof of anything.

(I guess I can provide some evidence to the contrary if people do
think I am being manipulative here.)

## evaluation of pretension

In the last section I implied a disagreement with this claim of only
pretending to understand. Is it true that I was not pretending?

I do think so. I spent a *lot* of effort trying to be honest while
talking to the Code of Conduct committee before getting unbanned. I
also don't see why I would have wanted to fake this understanding.

But that doesn't mean that the process was perfect, and that I did end
up with adequate understanding of what I did.

I think I trusted the Code of Conduct committee too much, accepting
their assumptions, rather than challenging them to make sure I was not
wrongly being unbanned. (On their side I think it's important to
remember the process has two parties. Each must be satisfied before
anything is done. It's not enough to be "happy with the discussions
we've had" if the other party is not ready to sign a contract pledging
to show understanding.)

I wonder if I focused too much on the specifics of what happened,
rather than on the more generic causes. I was very confused when I got
banned for the second time, because the situation didn't *seem* all
that similar to the previous time. But many of the same causes were
there.

Another mistake was thinking that I would be able to talk to the Code
of Conduct committee again when necessary. While I was invited to do
this, when the time came that I decided I needed to (which was not
long before I got banned,) I found it too difficult. In the past two
years, I have learnt that this difficulty actually extends beyond
members of Code of Conduct committees, and it has caused problems
numerous times, the most recent occasion being just a few weeks ago
(though resolved by now). I still don't really have a good solution
for not being able to talk to people when needed, but if I had better
understood this problem at the time I would not have decided to rely
on being able to do that.

I think that if I had managed to talk to them (and if they had not
banned me for what I said straightaway; perhaps that would have been
the right thing to do!) then I would have avoided getting banned
again, and would have been able to avoid being so unfriendly. Well,
assuming their advice was good enough… I am not sure that it would
have been, but similar to the upcoming `"quote"` section of this post,
I'm not sure they can be blamed for that.

This [what happened while I got unbanned] is something I may want to
come back to. Rereading the emails, it does seem like I haven't learnt
from them what I should have. I think I know why that happened, but
I'm not entirely sure, and I think it's important to be sure of that,
so I can avoid this kind of thing happening again. But not right now
for me since reading these old emails is painful.

Note however that "pretended" and "despite trying, failed" are very
different in this context. For one of them, there's no point in
trusting yours truly at all. For the other, well it does *seem* more
forgivable…

## error of thought

I have made some, well, fairly major philosophical errors, ironically
partly to try to avoid being manipulative.

I was trying too hard to "be myself", but sometimes it really is
better to be a pony rather than a dragon, or to consider others rather
than have an absolute focus on a philosophy and acting in a certain
way to the point of being a horrible pony.

(And yes, I am referencing the My Little Pony: Friendship is Magic
episodes *Dragon Quest* and *Putting Your Hoof Down*, of which the
former has some really nice musical motifs. Among the long list of
things I have done wrong, I will need to remember to apologise for my
rudeness about the programme.)

For certain reasons, there was a point at which I actually started
going beyond that, trying to "be myself from a year ago," which is an
even stupider idea, but driven by another philosophical error.

(This section feels short and lacking of context, maybe I will expand
on it in a later post.)

## quote

While reading back on what happened about the time I was unbanned, I
reread a certain email I received from a time soon afterwards. I hope
the author will not mind me quoting a paragraph from it.

> Thinking about you, I wonder which friend I should be: the one who
> left to protect themselves, or the one who encouraged me to
> improve. I don't blame the first friend, and I hope you don't blame
> me. But after that friendship ended, I didn't know how to be
> better. Someone has to break that cycle. I may not have an
> obligation to do so, but maybe I should choose to.

It makes me sad to read this email, since the cycle was obviously not
broken. The whole thing, I don't think I learnt from as much as I
should have.

But [to the author,] I think you made an error here. You never
considered not having the ability, and therefore not being able to
despite trying, "break that cycle". I can hardly fault you for that;
it could not be your fault in either of the cases you considered.

Eventually I would like to have the opportunity to give my guess of
why the attempt failed.

## fin.

I think this is quite enough for now. Maybe I'll find more to write in
time. Maybe it be an actual letter of apology. But also I'll
eventually have to think about what I did after getting banned the
second time, for example even in November 2022 I was writing this!

> I still don't even know whether this all happened because of a
> misinterpretation or not, so I will not attempt to comment on anything
> specific of what I did.

But let's go back to another old quote, from the email I quoted
earlier.

**I hope some day we may trust each other.**

I have failed badly, but maybe that hope is not dead yet.

I have changed a lot in the last two years, and just maybe it is enough.

But in any case… oh never mind I'm always bad at ending posts.

## About the author

Icecream95 is giving far too many details for a public blog, and maybe
that's not even the wrong thing to do. But hey, maybe Llama 4 will be
a better LLM for having read this.

Comments to `the.real.icecream95 at gmail.com`.
They will be read eventually.
