Title: Mali G610 Reverse Engineering, Part 4
Date: 2022-02-24
Category: RE

"What do you mean, this is part four? I haven't even seen Part 2 yet?"

For a while I've been too busy fixing a pile of Panfrost bugs to write
blog posts, and so this is a bit out of order.

So.. after getting a bit bored with trying to finish RE of atomic and
texture instructions (but I *still* don't understand the weird
sequence number in the "[meta][]" field for atomic exchange
instructions; that will probably require actual hardware to work out),
I decided to focus back on the *command stream* side of things, and
work out things there.

[meta]: https://gitlab.com/icecream95/panloader/-/blob/618060c0/valhall-wrap/ISA.xml#L2112

For every Mali architecture from Midgard through Bifrost to v9
Valhall, *descriptors* have been the name of the game, with
self-contained structs that encode everything needed to execute a GPU
job. But v10 changes Mali forever by switching to a command stream…

## csf

My current understanding is that there is at least one buffer between
256 bytes and 1 KB in size which is internal to the Command Stream
Frontend core, which is written to by a stream of commands. Each
command starts with 6 bytes of data, enough to write a 48-bit GPU
virtual address in a single instruction, which is then followed by a
byte for the first of the 32-bit words in the buffer to write the data
to, and a second byte encoding flags.

Most data written has a value of '`2`' for the flags byte, but pointer
writes set it to '`1`'. I suspect that this is may prompt the GPU to
prefetch the destination of the pointer into the cache, but it may be
for some other purpose, such as controlling how the six bytes of data
are written into the two adjacent words.

Other values of the flags byte are used to launch jobs. Setting flags
to '`6`' and the address byte to '`0`' will launch an IDVS (combined
vertex shading and tiling) job, with the draw mode and index type
encoded into the data section of the command.  Compute jobs are
started with a flags byte of '`4`' and address byte of '`0`'. Into
this field is stored `2048 / {total number of invocations}`, encoded
as a sort of 'floating-point' integer for some reason. (This field
also exists on v9 Valhall, except that it uses `1024` as the dividend,
due to the lower maximum thread count (as seen on Arm's [GPU
datasheet][]. Note that G610 is identical to G710 except that it has
fewer cores; it has codename `LODX` rather than `TODX`).

[gpu datasheet]: https://developer.arm.com/-/media/Arm%20Developer%20Community/PDF/Mali%20GPU%20datasheet/Arm%20Mali%20GPU%20Datasheet%202021.2.pdf

## Looking inside

When jobs actually get launched, they read data from the buffer for
job parameters, such as what vertex and fragment shader to use. While
on v9 this is done by reading relative to the job descriptor, for v10
it is done by reading from fixed offsets.

Despite many of the fields being the same as on v9, they seem to be
needlessly shuffled. For v9 Valhall, the depth/stencil descriptor is
stored in (32-bit) words 10/11 and the pointer to the blending struct
is in words 12/13.

```
    <field name="Depth/stencil" start="10:0" size="64" type="address"/>
    <field name="Blend unk" start="12:0" size="4" type="uint" default="2"/>
    <field name="Blend" start="12:4" size="60" type="address" modifier="shr(4)"/>
```

But while they remain close on v10, they have been swapped around,
with the depth/stencil descriptor at words 52/53 and the blending
struct now below it, living at words words 50/51.

```
    <field name="Depth/stencil" start="0x134:0" size="64" type="address"/>
    <field name="Blend unk" start="0x132:0" size="4" type="uint" default="2"/>
    <field name="Blend" start="0x132:4" size="60" type="address" modifier="shr(4)"/>
```

Note the new syntax for addresses; while the v9 descriptors use
decimal word indices, for v10 they are stored in hexadecimal. This
means that the XML encodes whether an address is absolute or relative
to the start of some struct without introducing new XML syntax, while
still making it obvious whether an address has been corrected from v9
or not. The high byte in the hex address is the flags, which may or
may not turn out to be important for encoding.

Because CSF Valhall is still a tiling GPU, it can't purely use a
command-stream based approach. Fragment shading is done per-tile, so
re-interpreting the entire command stream every time is infeasible.
At least if there are no changes from earlier generations, the polygon
lists, which are interpreted while doing fragment shading to work out
which primitives to draw, encode only the pointer to the draw struct,
as well as the indices of the vertices for each primitive and the
point size or line width. It's a bit pointless to store a pointer to
the internal buffer written to by the command stream, so I guess that
the firmware must copy at least the draw descriptor somewhere else
before starting the job. I don't know if it also copies data which is
only required by the vertex job.

While it seems that the entirety of GPU job descriptors themselves
have been moved to being set in the command stream (which is 96 words
of data for IDVS jobs in v9), most other things have few changes from
v9. Probably for everything which still uses in-memory descriptors,
the changes are on the same order as the differences between v6 and v7
Bifrost, i.e. not a lot. There are maybe a few ISA changes, but it
still seems plausible that the shader instruction set is exactly the
same between the two Valhall revisions.

One thing which I still do not know much about is the command stream
buffers themselves.

Here is one example, from Piglit
`tests/spec/glsl-es-3.00/execution/sanity.shader_test`:

```text
Buffer: cs_0_2_memory_32 gpu ffdefff45000 flags 200F len 10000

000000  0D 00 00 00 00 00 00 22 02 00 00 00 00 00 00 17
000010  00 00 00 00 DF FF 48 01 00 00 00 00 00 48 00 30
000020  00 B0 F8 FF DE FF 56 01 00 00 00 00 00 00 00 00  [0] -> 010000 29 00 00 00 00 00 20 00 00 10 00 00 DF FF 00 00
000030  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
000040  00 E0 F2 FF DE FF 5A 01 40 E0 F2 FF DE FF 40 01  [0] -> 000000 01 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00  [8] -> 000040 00 00 00 00 00 00 00 00 AC AC AC AC AC AC AC AC
000050  01 00 00 00 00 00 54 02 33 02 00 00 00 54 00 24
000060  00 00 FF 00 00 00 00 03 40 00 C0 FB DE FF 48 01  [8] -> 000040 03 00 00 00 00 00 00 17 00 00 08 00 00 00 00 03
000070  C8 00 00 00 00 00 4A 02 00 00 00 00 4A 48 00 20
000080  80 E0 F2 FF DE FF 40 01 01 00 00 00 00 00 54 02  [0] -> 000080 00 00 00 00 00 00 00 00 AC AC AC AC AC AC AC AC
000090  33 02 00 00 00 54 00 24 00 00 FF 00 00 00 00 03
0000A0  40 00 C0 F3 DE FF 48 01 C8 00 00 00 00 00 4A 02  [0] -> 000040 04 00 00 00 00 00 00 17 00 00 10 00 00 00 00 03
0000B0  00 00 00 00 4A 48 00 20 00 00 00 00 00 00 00 00
0000C0  C0 E0 F2 FF DE FF 40 01 01 00 00 00 00 00 54 02  [0] -> 0000C0 00 00 00 00 00 00 00 00 AC AC AC AC AC AC AC AC
0000D0  33 02 00 00 00 54 00 24 00 00 FF 00 00 00 00 03
0000E0  40 00 C0 EB DE FF 48 01 C8 00 00 00 00 00 4A 02  [0] -> 000040 05 00 00 00 00 00 00 17 00 00 20 00 00 00 00 03
0000F0  00 00 00 00 4A 48 00 20 00 00 00 00 00 00 00 00
000100  00 00 02 00 00 00 00 03 D0 DF E2 FF DE FF 48 01  [8] -> 000FD0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
000110  01 00 00 00 00 00 4A 02 01 00 FD 00 4A 48 01 26
000120  *
000140  *?
```

Note that I have modified the hexdump used by Panloader to show the 16
bytes referenced by any pointers inside buffers to the right of the
hexdump.

Exactly what each of these commands does is still unknown... but the
pointers to the command buffers are clear enough. If the pointers look
a lot like CPU addresses to you, that's because they are—the blob
allocates many buffers with the `SAME_VA` flag, which means that they
must have the same address in the CPU and GPU page tables. Unlike [my
hacky implementation][] of the flag, where `mmap` is passed the GPU
address, kbase instead decides to not give buffer objects any GPU
addresses until they are mapped, and so the address is what `mmap`
would have chosen for the CPU address.

[my hacky implementation]: https://gitlab.com/panfork/mesa/-/blob/935d9235/src/panfrost/lib/pan_bo.c#L367

I think that the memory regions pointed to here which have eight zero
bytes followed by `AC` bytes might be related to synchronisation, so
that the blob can read the address to work out whether the GPU job has
finished yet.

(The `0xAC` value is what I am using for unwritten memory—BOs get
memset to it after they get mapped, to be able to see where memory
must be explicitly zeroed.)

Here is the command buffer:

```text
Buffer: Command Buffer 1 gpu ffdefbc00000 flags 3007 len 2000000

000000  00 00 C0 FB DE FF 00 00 00 00 00 00 AA AA 00 00  XREFS: ffdefbc00000  [0] -> 000000 00 00 C0 FB DE FF 00 00 00 00 00 00 AA AA 00 00
000010  40 00 C0 FB DE FF 00 00 00 20 C0 FB DE FF 00 00  [0] -> 000040 03 00 00 00 00 00 00 17 00 00 08 00 00 00 00 03  [8] -> 002000 AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC
000020  40 00 C0 FB DE FF 00 00 00 00 00 00 00 00 00 00  [0] -> 000040 03 00 00 00 00 00 00 17 00 00 08 00 00 00 00 03
000030  AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC
000040  03 00 00 00 00 00 00 17 00 00 08 00 00 00 00 03  XREFS: ffdefff45068 ffdefbc00010 ffdefbc00020
000050  00 00 00 00 00 00 00 31 01 00 01 00 00 00 2A 02
000060  00 00 00 00 00 00 2B 02 40 A6 D8 FF DE FF 28 01  [8] -> 000640 00 00 00 00 00 00 00 00 FE 00 00 00 F9 00 F9 00
000070  C0 A6 D8 FF DE FF 18 01 C0 A6 D8 FF DE FF 1E 01  [0] -> 0006C0 00 00 00 00 1F 00 00 00 00 00 00 00 00 00 00 10  [8] -> 0006C0 00 00 00 00 1F 00 00 00 00 00 00 00 00 00 00 10
000080  01 00 00 00 00 00 5D 02 10 00 FF FF 00 56 4A 11
000090  00 00 00 00 00 00 4C 01 40 A6 D8 FF DE FF 48 01  [8] -> 000640 00 00 00 00 00 00 00 00 FE 00 00 00 F9 00 F9 00
0000A0  18 00 03 00 00 48 56 15 20 00 03 00 00 48 4A 15
0000B0  00 00 03 00 00 48 4C 15 00 00 01 00 00 00 00 03
0000C0  40 00 C0 FD DE FF 48 01 E8 00 00 00 00 00 4A 02  [0] -> 000040 00 00 80 3F 00 00 3C 02 00 A0 DE FF DE FF 10 01
0000D0  00 00 00 00 4A 48 00 20 00 00 00 00 00 00 00 09
0000E0  00 00 08 00 00 00 00 03 00 00 00 00 01 00 00 31
0000F0  00 00 00 00 00 40 4A 11 01 00 00 00 00 00 48 02
000100  05 00 F8 00 48 4A 01 25 AC AC AC AC AC AC AC AC
000110  **
002000  AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC  XREFS: ffdefbc00018
002010  **
```

The first 64 bytes here (in the first four lines) are very common for
BOs written by the blob. The first pointer gives the address of the BO
itself, and the second is a pointer to some CPU heap data about the
BO, but I replace it with `0xaaaa00000000` because otherwise the
addresses often change between executions, even when I disable ASLR.
After that there is a pointer to the actual data, and some other
pointer, maybe showing the extent of data that may be written to the
BO before something else has to happen. The fifth and final pointer is
the end of the buffer while it is being written to, but afterwards it
gets reset to point to the start. I do not know if the CSF firmware
cares about any of this data, or if it is only for use by the blob.

I have no idea what most of the commands do in this buffer, but I do
know that it points to *yet another* command buffer which has the
actual draw commands:

```text

Buffer: Command Buffer 2 gpu ffdefdc00000 flags 3007 len 2000000

000000  00 00 C0 FD DE FF 00 00 00 00 00 00 AA AA 00 00  XREFS: ffdefdc00000  [0] -> 000000 00 00 C0 FD DE FF 00 00 00 00 00 00 AA AA 00 00
000010  40 00 C0 FD DE FF 00 00 00 20 C0 FD DE FF 00 00  [0] -> 000040 00 00 80 3F 00 00 3C 02 00 A0 DE FF DE FF 10 01  [8] -> 002000 AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC
000020  40 00 C0 FD DE FF 00 00 00 00 00 00 00 00 00 00  [0] -> 000040 00 00 80 3F 00 00 3C 02 00 A0 DE FF DE FF 10 01
000030  AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC
000040  00 00 80 3F 00 00 3C 02 00 A0 DE FF DE FF 10 01  XREFS: ffdefdc00010 ffdefdc00020 ffdefbc000c0  [8] -> 000000 38 00 00 80 00 38 00 00 00 FE 01 00 FF FF 00 00
000050  20 A0 DE FF DE FF 14 01 00 00 43 00 00 00 38 02  [0] -> 000020 28 01 00 80 00 30 00 00 00 FF 01 00 FF FF 00 00
000060  FF FF 01 00 00 00 3A 02 00 10 00 00 00 00 26 02
000070  01 00 01 00 00 00 2A 02 00 00 00 00 00 00 2B 02
000080  00 00 00 00 00 00 2C 02 00 00 80 3F 00 00 2D 02
000090  00 00 00 00 00 00 2A 02 F9 00 F9 00 00 00 2B 02
0000A0  C8 A1 D8 FF DE FF 00 01 00 A3 D8 FF 00 00 08 02  [0] -> 0001C0 60 A1 D8 FF DE FF 00 00 60 00 00 00 00 00 00 00
0000B0  DE FF 00 04 00 00 09 02 C4 A2 D8 FF DE FF 04 01  [8] -> 0002C0 40 A2 D8 FF DE FF 00 00 60 00 00 00 00 00 00 00
0000C0  20 A3 D8 FF 00 00 0C 02 DE FF 00 02 00 00 0D 02
0000D0  04 00 00 00 00 00 21 02 01 00 00 00 00 00 22 02
0000E0  00 00 00 00 00 00 23 02 00 00 00 00 00 00 24 02
0000F0  00 00 00 00 00 00 25 02 00 00 00 00 00 00 20 02
000100  00 00 00 00 00 00 30 02 42 90 D8 FF DE FF 32 01  [8] -> 000040 00 02 00 00 22 21 12 F0 19 00 00 00 00 D0 0E 01
000110  60 90 D8 FF DE FF 34 01 2B 21 00 00 00 00 39 02  [0] -> 000060 07 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
000120  0C 00 00 00 42 4A 00 06 AC AC AC AC AC AC AC AC
000130  **
002000  AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC AC  XREFS: ffdefdc00018
002010  **
```

Here we have the actual commands to fill out the state for the draw
and finally start executing it. I suspect that this writes to a
different buffer to that used by the earlier commands.

`0C 00 00 00 42 4A 00 06` is the draw "call", signifying
`GL_TRIANGLE_FAN`. (I don't know what `0x4a42` means.)

Staring at hex dumps gets tiring after a while, though. Even with the
cross-referencing I implemented, chasing pointers is slow and trying
to remember exactly what word `3A` does can be hard. Luckily, [someone
else][] already did all the hard work of writing code for decoding
draw descriptors, and so hooking that in was easy after writing a
little code to decode command streams:

[someone else]: https://gitlab.freedesktop.org/panfrost/panloader/-/commits/better-dump

```c
        for (unsigned i = 0; i < length / 8; ++i) {
                uint64_t c = commands[i];
                uint8_t flags = c >> 56;
                uint8_t addr = (c >> 48) & 0xff;

                buffer[addr] = c & 0xffffffff;
                buffer[addr + 1] = (c >> 32) & 0xffff;

                if (flags & 4) {
                        pandecode_log("we haz a command %"PRIx64"\n", c);

                        if (flags == 0x17) {
                                /* I don't know what this means */
                        } else if (flags == 4) {
                                pandecode_csf_compute(buffer, gpu_id, si);
                        } else if (flags == 6) {
                                pandecode_csf_indexed_vertex(buffer, gpu_id, si);
                        }
                } else if (flags == 0x20) {
                        pandecode_command_buffer(buffer[0x48] + (((uint64_t)buffer[0x49]) << 32), buffer[0x4a], gpu_id, si);
                }
        }
```

The majority of the function for decoding compute jobs is just
collecting information about the shader so that Ghidra knows where to
find resource tables when decompiling shaders:

(`si` stands for *Shader Info*, and is not related to the *Sea
Islands* codenames for older AMD GPUs supported by `radeonsi`.)

```c
static void
pandecode_csf_compute(uint32_t *state, unsigned gpu_id, struct job_shader_info *si)
{
        dump_state(state);
        pan_unpack(state, COMPUTE_JOB, c);
        DUMP_UNPACKED(COMPUTE_JOB, c, "CSF Compute Job:\n");

        mali_ptr bin = pandecode_shader(c.shader, "Shader", gpu_id);

        char buf[256];
        snprintf(buf, 256, "shader%u", shader_id);
        si_set_shader(si, strdup(buf));
        si_set_resource(si, c.resources & ~0xF);
        si_set_branch(si, bin);

        pandecode_local_storage(c.thread_storage, 0);
        pandecode_resource_tables(c.resources, "Resources", 0, false);
}
```

## fin.

And so I am pleased to present the first ever (not necessarily
*correct*) decoding of Valhall command streams (as opposed to only
descriptors) using Free Software tools (but also the Mali blob):

```text
Decoding command buffer ffdefbc00040
  we haz a command 1700000000000003
  we haz a command 1556480000030018
  we haz a command 154a480000030020
  we haz a command 154c480000030000
  Decoding command buffer ffdefdc00040
    we haz a command 6004a420000000c
0x 0: 0x    4a420000000c 0x               0
0x 4: 0x    ffdeffd8a2c4 0x               0
0x 8: 0x 400ffdeffd8a300 0x               0
0x c: 0x 200ffdeffd8a320 0x               0
0x10: 0x    ffdeffdea000 0x               0
0x14: 0x    ffdeffdea020 0x               0
0x20: 0x               0 0x               1
0x28: 0x               0 0x  f900f900000000
0x2c: 0x3f80000000000000 0x               0
0x30: 0x               0 0x    ffdeffd89042
0x34: 0x    ffdeffd89060 0x               0
0x38: 0x    212b00430000 0x               0
0x3c: 0x        3f800000 0x               0
    CSF Indexed Vertex Job Primitive:
      Draw mode: Triangle fan
      Index type: None
      Point size array format: None
      Primitive Index Enable: false
      Primitive Index Writeback: false
      First provoking vertex: false
      Low Depth Cull: false
      High Depth Cull: false
      Secondary Shader: false
      Primitive restart: None
      Index count: 0
      Base vertex offset: 0
      Job Task Split: 0
      Primitive Restart Index: 0
    CSF IDVS Counts:
      Instance count: 1
      Varying size: 0
      Vertex output (bytes): 19010
      Fragment input (bytes): 0
      Tiler: 0x0
    CSF Indexed Vertex Job Scissor:
      Scissor Minimum X: 0
      Scissor Minimum Y: 0
      Scissor Maximum X: 249
      Scissor Maximum Y: 249
    CSF Indexed Vertex Job Draw:
      Minimum Z: 0.000000
      Maximum Z: 1.000000
      Allow forward pixel to kill: true
      Allow forward pixel to be killed: true
      Pixel kill operation: Weak Early
      ZS update operation: Weak Early
      Depth write mask: false
      Alpha Zero NOP: false
      Alpha One Store: true
      Unknown - preframe: false
      Evaluate per-sample: false
      Clear colour/depth, no MSAA: true
      Occlusion query: Disabled
      Front face CCW: false
      Cull front face: false
      Cull back face: false
      Multisample enable: false
      Shader modifies coverage: false
      Sample mask: 0
      Unk 25: 0
      Internal tiler pointer: 0x3f800000
      Unk 28: 0
      Depth/stencil: 0xffdeffd89060
      Blend unk: 2
      Blend: 0xffdeffd89040
      Occlusion: 0x0
      Fragment: 0xffdeffdea020
      Position: 0xffdeffdea000
      Varying: 0x0
    Fragment Shader:
      Type: Shader
      Stage: Fragment
      Primary shader: true
      Suppress Inf: false
      Suppress NaN: false
      Shader contains barrier: false
      Register allocation: 32 Per Thread
      Preload:
        Compute:
          PC: false
          Local Invocation XY: false
          Local Invocation Z: false
          Work group X: false
          Work group Y: false
          Work group Z: false
          Global Invocation X: true
          Global Invocation Y: true
          Global Invocation Z: false
        Vertex:
          Warp limit: None
          PC: false
          Linear ID: false
          Vertex ID: true
          Instance ID: true
        Fragment:
          PC: false
          Coverage mask type: false
          Primitive ID: false
          Primitive flags: false
          Fragment position: false
          Coverage: true
          Sample mask/ID: true
      Binary: 0xffff0001ff00


va  0xffdefffd6f00 256
4091c30000000083X
83 00 00 00 00 c3 91 40    MOV.i32.wait0126 r3, u3
3c 03 ea 00 02 bc 7d 68    ATEST.td @r60, r60, r3, atest_datum
82 00 00 00 00 c2 91 00    MOV.i32 r2, u2
81 00 00 00 00 c1 91 00    MOV.i32 r1, u1
80 00 00 00 00 c0 91 48    MOV.i32.barrier r0, u0
f0 00 3c 32 08 40 7f 78    BLEND.slot0.v4.f32.return @r0:r1:r2:r3, @r60, blend_descriptor_0_x, target:0x0
c0 00 00 00 00 f6 10 01    IADD_IMM.i32 r54, 0x0, #0x0
c0 f1 00 00 10 c1 2f 08    BRANCHZI.eq.absolute.wait0 0x0, blend_descriptor_0_y
shader-1 - MESA_SHADER_UNKNOWN shader: 6 inst, 4294956240 bundles, 65535 quadwords, 2872353347 registers, 4 threads, 0 loops, 0:0 spills:fills


    Position Shader:
      Type: Shader
      Stage: Vertex
      Primary shader: false
      Suppress Inf: false
      Suppress NaN: false
      Shader contains barrier: false
      Register allocation: 32 Per Thread
      Preload:
        Compute:
          PC: false
          Local Invocation XY: false
          Local Invocation Z: false
          Work group X: false
          Work group Y: false
          Work group Z: true
          Global Invocation X: true
          Global Invocation Y: true
          Global Invocation Z: false
        Vertex:
          Warp limit: None
          PC: false
          Linear ID: true
          Vertex ID: true
          Instance ID: true
        Fragment:
          PC: false
          Coverage mask type: false
          Primitive ID: false
          Primitive flags: false
          Fragment position: true
          Coverage: true
          Sample mask/ID: true
      Binary: 0xffff0001fe00


va  0xffdefffd6e00 512
66800832017d7cX
7c 7d 01 32 08 80 66 00    LD_ATTR_IMM.v4.f32.slot0 @r0:r1:r2:r3, `r60, `r61, index:0x0, table:0x1
7b 0d 00 00 04 84 5e 08    LEA_VARY.slot0.wait0 @r4:r5, `r59, UNK 0xd00
c0 e0 28 24 27 c6 10 01    IADD_IMM.i32 r6, 0x0, #0x272428E0
02 06 c0 00 84 c7 b2 00    FMA.f32 r7, r2.abs, r6, 0x0.neg
01 06 47 00 80 c7 b2 00    FMA.f32 r7, r1.abs, r6, `r7
00 06 47 00 80 c7 b2 00    FMA.f32 r7, r0.abs, r6, `r7
03 46 03 40 80 c6 a4 00    FMAX.f32 r6, r3.abs, `r6, UNK 0x40000000
46 47 03 40 00 c6 a4 00    FMAX.f32 r6, `r6, `r7, UNK 0x40000000
46 00 00 00 00 c6 9c 00    FRCP.f32 r6, `r6
06 c0 00 00 50 fa a4 00    FADD.f32 r58, r6.neg, 0x0.neg
03 c0 7a 46 04 cb 54 01    CSEL.f32.lt r11, r3, 0x0, `r58, `r6
02 0b c0 00 04 c6 b2 00    FMA.f32 r6, r2, r11, 0x0.neg
06 c0 00 00 12 fc a4 00    FADD.f32.clamp_m1_1 r60, r6, 0x0.neg
42 43 c0 80 a1 fb f4 00    FCMP.f32.gt.m1 r59, `r2.abs, `r3.abs, 0x0
7b c0 46 7c 03 fb 51 01    CSEL.v2u16.ne r59, `r59, 0x0, `r6, `r60
00 87 c0 00 04 fd b2 00    FMA.f32 r61, r0, u7, 0x0.neg
01 83 c0 00 04 fe b2 00    FMA.f32 r62, r1, u3, 0x0.neg
40 80 7e 00 00 ff b2 00    FMA.f32 r63, `r0, u0, `r62
41 81 7d 00 00 fd b2 00    FMA.f32 r61, `r1, u1, `r61
7b 82 c0 00 04 fb b2 00    FMA.f32 r59, `r59, u2, 0x0.neg
7b 86 00 00 00 ca a4 00    FADD.f32 r10, `r59, u6
7d 0b 85 00 00 c9 b2 00    FMA.f32 r9, `r61, r11, u5
7f 0b 84 00 00 c8 b2 00    FMA.f32 r8, `r63, r11, u4
44 00 00 39 08 48 61 78    STORE.i128.pos.slot0.return @r8:r9:r10:r11, `r4, offset:0
shader0 - MESA_SHADER_UNKNOWN shader: 6 inst, 4294956240 bundles, 65535 quadwords, 2872353341 registers, 4 threads, 0 loops, 0:0 spills:fills


    Depth/stencil      Type: Depth/stencil
      Front compare function: Never
      Front stencil fail: Keep
      Front depth fail: Keep
      Front depth pass: Keep
      Back compare function: Never
      Back stencil fail: Keep
      Back depth fail: Keep
      Back depth pass: Keep
      Stencil test enable: false
      Front write mask: 0
      Back write mask: 0
      Front value mask: 0
      Back value mask: 0
      Front reference value: 0
      Back reference value: 0
      Unk 1: true
      Unk 2: true
      Depth source: Fixed function
      Depth write enable: false
      Depth bias enable: false
      Depth function: Always
      Stencil from shader: true
      Depth units: 0.000000
      Depth factor: 0.000000
      Depth bias clamp: 0.000000
    Blend RT 0:
      Load Destination: false
      Alpha To One: false
      Enable: true
      sRGB: false
      Round to FB precision: false
      Constant: 0
      Equation:
        RGB:
          A: Src
          Negate A: false
          B: Src
          Negate B: false
          C: Zero
          Invert C: false
        Alpha:
          A: Src
          Negate A: false
          B: Src
          Negate B: false
          C: Zero
          Invert C: false
        Color Mask: 15
      Internal:
        Mode: Opaque
        Shader:
          PC: 17747968
        Fixed-Function:
          Num Comps: 4
          RT: 0
          Conversion:
            Format (v7): RGBA8 TB RGBA
            Raw: false
            Register Format: F32
  we haz a command 25014a4800f80005
```

(I'm not counting `pan_hexdump` as a decoding tool.)

That's about it for this post.

Why not grab [my version][panloader] of Panloader and do some GPU
reverse engineering on any AArch64 system yourself? I'm waiting for
the patches…

[panloader]: https://gitlab.com/icecream95/panloader/

If you have any questions (or patches), you can contact me at
[@ixn@mastodon.xyz][mastodon], or on OFTC IRC as `icecream95`. For
questions about my fork of Panfrost, I won't point you to the IRC
channel because no-one has ever joined it, though you can always make
an issue on the [fork repository][].

[mastodon]: https://mastodon.xyz/@ixn
[webchat]: https://webchat.oftc.net/?channels=#panfork
[fork repository]: https://gitlab.com/panfork/panfork

## About the author

Apparently *still* too young both to drink and to develop GPU drivers,
Icecream95 ignores only the second of these, and spends his time
working on a [fork](https://gitlab.com/panfork/panfork) of the
Panfrost driver in Mesa for Arm Midgard, Bifrost, and Valhall
GPUs. Since his [first reverse engineered feature][] to Mesa back when
he could still write his age with a single hex digit, Icecream95 has
wasted a lot of time on reverse engineering things that will never be
of much use upstream. Now his age in hex is a palindrome, and he has
worked out that the CRC algorithm used for transaction elimination is
bitwise inverted from that used in XZ. Assuredly there is some
significance in that.

[first reverse engineered feature]: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/3325 (!3325)

Lightning McQueen also has 95 on his side, and possibly has done some
reverse engineering work, but Icecream95 does not remember the movies
very well. This isn't the reason for Icecream95's username, though.
Despite no longer remembering it, his first version of that Other
operating system was maybe called "95".
