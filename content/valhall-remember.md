Title: Maybe I misremember
Date: 2022-11-09
Category: Screenshots
Summary: Memory isn't always reliable

<img src="/images/valhall-works.png" width="760"/>

Maybe I misremember.

![](/images/valhall-works-crop.png)

I guess it could have been the vendor driver, I dunno.

![](/images/valhall-works-crop2.png)

Perhaps a joke using panloader to change the renderer string?

![](/images/valhall-works-oops.png)

Hmm maybe not then, because vendor drivers don't give artefacts like that.

![](/images/valhall-works-blob.png)

Not most of the time, anyway. Not unless someone messed up LPDDR4X initialisation code.

<https://gitlab.com/panfork/mesa>

(Note that wlroots doesn't call `eglBindWaylandDisplayWL`, which I use
to start exposing the `mali_buffer_sharing` protocol, so it is
impossible for applications to use the blob when running on Sway. All
but the last of these screenshots are from Sway.)

## fin.

A note to upstream: I'm still confused about what's going on, and it
seems that users are too. When Christmas comes with its presents, are
users going to be even more confused from having to choose between
*three* drivers?

It'd be nice to work out some way to end this fragmentation, and to
have peace again. I seem to be powerless here, so until something
happens I can only complain and (constantly) be worried.
I still don't even know whether this all happened because of a
misinterpretation or not, so I will not attempt to comment on anything
specific of what I did.

## About the author

Although not taking advantage of being old enough to drink, the jury
may still be out on whether Icecream95 is too young to develop GPU
drivers. Icecream95 can only wonder, because even the judges seem to
be using his emails as bog roll. But occasionally managing to ignore
all of that, Icecream95 spends his time working on a fork of the Lima
and Panfrost drivers in Mesa for Arm Utgard and Valhall GPUs, both on
ancient vendor kernels, though only one of them is 3.10.76. Icecream95
has recently learnt that that the Mali100 was undergoing testing at a
time suspiciously close to his birth, and wonders if a shard of its
gourad shading circuits got accidentally embedded inside him.

Lightning McQueen also has 95 on his side, and many integrated
circuits, though perhaps not for Gourad shading. This isn't the reason
for Icecream95's username, though. The real reason is because the
Utgard tiler heap contains addresses shifted right both 6 and 11 bits,
and 6×14 + 11 = 95. Don't ask where the 14 came from.
